// ---------------------------------------------------------------------------------------------------------------------
// Message Logging Middleware
// ---------------------------------------------------------------------------------------------------------------------

import {
    OperationMiddleware,
    Request,
    RequestEnvelope,
    ResponseEnvelope,
    StrataClient,
    logging,
} from '@strata-js/strata';

// Workaround for lodash not supporting esm imports
import pkg from 'lodash';
const { cloneDeep } = pkg;

// ---------------------------------------------------------------------------------------------------------------------

export interface MessageLoggingConfig
{
    serviceGroup : {
        toMonitor : string;
        toLogTo : string;
    }
    endpoint ?: {
        context ?: string;
        operation ?: string;
    },
    exclusions ?: string[];
    logOnFailure ?: string[];
    shouldLogRequestFn ?: (request : Request) => boolean;
    transformRequestFn ?: (request : RequestEnvelope) => void;
    transformResponseFn ?: (response : ResponseEnvelope) => void;
}

const logger = logging.getLogger('logging-middleware');

// ---------------------------------------------------------------------------------------------------------------------

export class MessageLoggingMiddleware implements OperationMiddleware
{
    #client : StrataClient;
    #config : MessageLoggingConfig;

    constructor(client : StrataClient, config : MessageLoggingConfig)
    {
        this.#client = client;
        this.#config = config;
    }

    // -----------------------------------------------------------------------------------------------------------------

    _isInExclusions(request : Request) : boolean
    {
        const contexts = this.#config.exclusions;
        const context = `${ request.context }.*`;
        const endpoint = `${ request.context }.${ request.operation }`;

        return !!contexts && (contexts.includes(context) || contexts.includes(endpoint));
    }

    _isInLogOnFailure(request : Request) : boolean
    {
        const contexts = this.#config.logOnFailure;
        const context = `${ request.context }.*`;
        const endpoint = `${ request.context }.${ request.operation }`;

        return !!contexts && (contexts.includes(context) || contexts.includes(endpoint));
    }

    async _logRequest(request : Request) : Promise<void>
    {
        // Render the request
        let requestEnv = request.renderRequest();

        if(this.#config.transformRequestFn)
        {
            // Deep clone the request here to prevent logging transforms from modifying the actual request
            requestEnv = cloneDeep(requestEnv);
            this.#config.transformRequestFn(requestEnv);
        }

        // We are intentionally not waiting on the promise, so this function will finish as quickly as possible.
        // Note: We use the service's queue as it can only listen to one queue at a time, and we don't otherwise
        // know the incoming queue.
        this.#client.post(
            this.#config.serviceGroup.toLogTo,
            this.#config.endpoint?.context ?? 'monitor',
            this.#config.endpoint?.operation ?? 'logMessage',
            {
                queue: this.#config.serviceGroup.toMonitor,
                envelope: requestEnv,
            }
        )
            .catch((error) =>
            {
                logger.warn('Failed to log request:', error.stack);
            });
    }

    async _logResponse(request : Request) : Promise<void>
    {
        // Render the response
        let responseEnv = request.renderResponse();

        if(this.#config.transformResponseFn)
        {
            // Deep clone the response here to prevent logging transforms from modifying the actual response
            responseEnv = cloneDeep(responseEnv);
            this.#config.transformResponseFn(responseEnv);
        }

        // We are intentionally not waiting on the promise, so this function will finish as quickly as possible.
        this.#client.post(
            this.#config.serviceGroup.toLogTo,
            this.#config.endpoint?.context ?? 'monitor',
            this.#config.endpoint?.operation ?? 'logMessage',
            {
                queue: request.responseQueue,
                envelope: responseEnv,
            }
        )
            .catch((error) =>
            {
                logger.warn('Failed to log request:', error.stack);
            });
    }

    _shouldLogRequest(request : Request) : boolean
    {
        return !this._isInExclusions(request) && !this._isInLogOnFailure(request)
            && (!this.#config.shouldLogRequestFn || this.#config.shouldLogRequestFn(request));
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Middleware Implementation
    // -----------------------------------------------------------------------------------------------------------------

    async beforeRequest(request : Request) : Promise<Request | undefined>
    {
        if(this._shouldLogRequest(request))
        {
            await this._logRequest(request);
        }

        return request;
    }

    async failure(request : Request) : Promise<Request | undefined>
    {
        if(!this._isInExclusions(request))
        {
            if(this._isInLogOnFailure(request))
            {
                await this._logRequest(request);
            }

            await this._logResponse(request);
        }

        return request;
    }

    async success(request : Request) : Promise<Request | undefined>
    {
        if(this._shouldLogRequest(request))
        {
            await this._logResponse(request);
        }

        return request;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
