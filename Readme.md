# Strata Message Logging Middleware

Message logging middleware for use with a [Strata Service](https://gitlab.com/stratajs/strata).

_(See the [Strata Middleware](https://gitlab.com/stratajs/strata/-/blob/master/docs/middleware.md) documentation for 
more details on middleware.)_

The Message Logging Middleware is intended as a companion to the [Strata Queue Monitor][sqm] service, though it does not
require SQM. All the middleware does is log request and responses off to the specified queue (and optionally the 
specified context/operation) with the following format:

```typescript
interface LoggingMiddlewarePayload {
    queue: string;
    envelope: XOR<RequestEnvelope, ResponseEvelope>
}
```

Setting up a service to listen to the right place and take these payloads is beyond the scope of this middleware. 
(Though, the SQM service is designed to work with this middleware.)

## Installation

This middleware package is published via NPM's npm repository.

```bash
// npm
$ npm add @strata-js/middleware-message-logging

// yarn
$ yarn add @strata-js/middleware-message-logging
```

## Usage

### Setting up message logging

A validation configuration must be provided that contains, at a minimum, the queue to log the messages to. (This 
configuration may also specify the context/operation to log with, but defaults to 'monitor.logMessage', which is the 
default for SQM.)

We recommend you set this up as a global middleware, but it works at any level.

```typescript
import { service } from '@strata-js/strata';
import { MessageLoggingMiddleware } from '@strata-js/middleware-message-logging';

const loggingMiddleware = new MessageLoggingMiddleware({ loggingQueue: 'StrataQueueMonitor' });

// Register Middleware
service.useMiddleware(loggingMiddleware)

// ...

await service.init('LoggingExample');
```

### Specifying an alternative context/operation to log to

You can specify an alternative context/operation to log to very easily:

```typescript
import { service } from '@strata-js/strata';
import { MessageLoggingMiddleware } from '@strata-js/middleware-message-logging';

const loggingMiddleware = new MessageLoggingMiddleware({
    loggingQueue: 'StrataQueueMonitor',
    endpoint: {
        context: 'myLoggingContext',
        operation: 'myLoggingOperation'
    }
});

// Register Middleware
service.useMiddleware(loggingMiddleware)

// ...

await service.init('LoggingExample');
```

### Excluding some contexts/operations

You can exclude some contexts and/or operations by specifying an `exclusions` list. This is a list of strings, and 
must be in the form of either `'context.opertation'` or `'context.*'` if you want to ignore an entire context.

```typescript
import { service } from '@strata-js/strata';
import { MessageLoggingMiddleware } from '@strata-js/middleware-message-logging';

const loggingMiddleware = new MessageLoggingMiddleware({
    loggingQueue: 'StrataQueueMonitor',
    exclusions: [
        'myContext.myOperation',
        'secure.*',
        'superSecret.doNotExposeToLogging',
        'overusedContext.*'
    ]
});

// Register Middleware
service.useMiddleware(loggingMiddleware)

// ...

await service.init('LoggingExample');
```

[sqm]: https://gitlab.com/strata-js/tools/strata-queue-monitor
